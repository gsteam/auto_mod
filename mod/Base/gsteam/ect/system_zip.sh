#!/sbin/sh
#Nettoyage, prepa
[ -d /data/media/gsteam/prepa ] && rm -r /data/media/gsteam/prepa
busybox mkdir -p /data/media/gsteam/prepa/app

#Zip et Zipaligne
cd /data/media/gsteam/system_zip/app/
for source in $(ls)
do
  cp /system/app/$source /data/media/gsteam/prepa/app/
  cd /data/media/gsteam/system_zip/app/$source/
  /tmp/zip -r /data/media/gsteam/prepa/app/$source *
  cd /data/media/gsteam/prepa/app/
	for sortie in $(ls)
	do
		busybox mkdir zipalign
		/tmp/zipalign -f 4 $sortie ./zipalign/$sortie
	done
done

#Envoi su System
cd /data/media/gsteam/prepa/app/zipalign/
cp * /system/app/
chmod 644 /system/app/*

#Nettoyage
rm -r -f /data/media/gsteam
