#!/tmp/busybox sh
#
#Monte des partitions et modification database Contacts.Providers
#
set +x
mkdir /datadata 
/tmp/busybox mount /datadata
/tmp/sqlite3 /data/data/com.android.providers.contacts/databases/contacts2.db  'ALTER TABLE raw_contacts ADD COLUMN is_restricted INTEGER NOT NULL DEFAULT 0';
/tmp/sqlite3 /datadata/com.android.providers.contacts/databases/contacts2.db  'ALTER TABLE raw_contacts ADD COLUMN is_restricted INTEGER NOT NULL DEFAULT 0';
