#!/tmp/busybox sh
#
#D�monte des partitions, pause, reboot
#
set +x
/tmp/busybox umount -l /datadata
/tmp/busybox umount -l /data
/tmp/busybox umount -l /system
sleep 5
reboot
