#!/bin/sh

show_apktool_progress() {
	#Affiche la progression.
	#Parametres: i 'texte' ; i est un entier representant le nombre de lignes affichees par apktool
	process=$!
	progress=0
	echo -ne "\r"\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ "\c"
	echo -ne "\r"$2 ["\c"
	for i in $(seq $1); do
		echo -n " "
	done
	echo -n "]"
	echo -ne "\r"$2 ["\c"
	while ps | grep $process > /dev/null && [ $progress -lt $1 ] ; do
		while [ $(cat apktool.log | wc -l) -gt $progress ]; do
			echo -n "#"
			let progress++
		done
	done
	wait
	echo -ne "\r"\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ "\r\c"
}


#-------------------------------------------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------- NORMAL ------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------------------------------------------------------------------------

# Extraction, transfert, decompile apk
# ------------------------------------
unzip -qq -j $Rom_Zip \
  system/app/SystemUI.apk \
  -d $Temp_Dir
apktool d -f $Temp_Dir/SystemUI.apk $Temp_Dir/SystemUI > apktool.log 2>&1 & show_apktool_progress 8 'Decompilation'
	if grep "error" apktool.log  ; then  echo "$(date +\[%T\]) Erreur de decompilation de SystemUI.apk" | tee -a $logfile ; cat apktool.log | tee -a $logfile; return 1; else rm -f apktool.log; fi #quitte en cas d'erreur

cd $Work_Dir/tools

# Copie des fichiers modifie
# ---------------------------
 cp -r $Tools_Dir/add_SystemUI/copie/* $Temp_Dir/SystemUI > /dev/null 2>&1
 cp -r $Tools_Dir/add_SystemUI/copie_$Size/* $Temp_Dir/SystemUI > /dev/null 2>&1

 
# Modification des fichiers
# --------------------------

# -smali/com/android/SystemUI/statusbar/phone/PhoneStatusBar\$FastColorDrawable.smali
#sed -e /public\ draw/,/method/d \
#    $Temp_Dir/SystemUI/smali/com/android/systemui/statusbar/phone/PhoneStatusBar\$FastColorDrawable.smali > $Temp_Dir/SystemUI/smali/com/android/systemui/statusbar/phone/temp.smali
#cat $Temp_Dir/SystemUI/smali/com/android/systemui/statusbar/phone/temp.smali add_SystemUI/modif/modif_PhoneStatusBar\$FastColorDrawable.smali > $Temp_Dir/SystemUI/smali/com/android/systemui/statusbar/phone/PhoneStatusBar\$FastColorDrawable.smali
#rm -f $Temp_Dir/SystemUI/smali/com/android/systemui/statusbar/phone/temp.smali


# Compile et Signe
# ----------------
apktool b -f $Temp_Dir/SystemUI $Temp_Dir/MODSystemUI_G.apk > apktool.log 2>&1 & show_apktool_progress 3 'Compilation'
	if grep "error" apktool.log  ; then  echo "$(date +\[%T\]) Erreur de decompilation de SystemUI.apk" | tee -a $logfile ; cat apktool.log | tee -a $logfile; return 1; else rm -f apktool.log; fi #quitte en cas d'erreur

  
java -Xmx512m -jar $Tools_Dir/signapk.jar -w $Tools_Dir/testkey.x509.pem $Tools_Dir/testkey.pk8 $Temp_Dir/MODSystemUI_G.apk $Temp_Dir/SystemUI_G.apk > apktool.log 2>&1
	if grep "error" apktool.log  ; then  echo "$(date +\[%T\]) Erreur de signature de SystemUI.apk" | tee -a $logfile ; cat apktool.log | tee -a $logfile; return 1; else rm -f apktool.log; fi #quitte en cas d'erreur



# Envoi de l'apk compile/signe dans la base
#------------------------------------------
cp -f $Temp_Dir/SystemUI_G.apk $Work_Dir/Base_temp/gsteam/system/app/SystemUI.apk

#-------------------------------------------------------------------------------------------------------------------------------------------------------------
#-------------------------------------------------------------------- CENTRAGE DE L'HEURE --------------------------------------------------------------------
#-------------------------------------------------------------------------------------------------------------------------------------------------------------

# Copie des fichiers modifie Heure centr�e
# ---------------------------
cp -r $Tools_Dir/add_SystemUI/copie_heure/* $Temp_Dir/SystemUI > /dev/null 2>&1
 
# Compile et Signe
# ----------------
apktool b -f $Temp_Dir/SystemUI $Temp_Dir/MODSystemUI.apk > apktool.log 2>&1 & show_apktool_progress 3 'Compilation'
	if grep "error" apktool.log  ; then  echo "$(date +\[%T\]) Erreur de decompilation de SystemUI-centre.apk" | tee -a $logfile ; cat apktool.log | tee -a $logfile; return 1; else rm -f apktool.log; fi #quitte en cas d'erreur

  
java -Xmx512m -jar $Tools_Dir/signapk.jar -w $Tools_Dir/testkey.x509.pem $Tools_Dir/testkey.pk8 $Temp_Dir/MODSystemUI.apk $Temp_Dir/SystemUI.apk > apktool.log 2>&1
	if [ $? != 0 ]  ; then  echo "$(date +\[%T\]) Erreur de sygnature de SystemUI-centre.apk" | tee -a $logfile ; cat apktool.log | tee -a $logfile; return 1; else rm -f apktool.log; fi #quitte en cas d'erreur


# Envoi de l'apk compile/signe dans la base
#------------------------------------------
cp -f $Temp_Dir/SystemUI.apk $Work_Dir/Base_temp/gsteam/Heure/GSteam/SystemUI.apk
