#!/bin/sh

show_apktool_progress() {
	#Affiche la progression.
	#Parametres: i 'texte' ; i est un entier representant le nombre de lignes affichees par apktool
	process=$!
	progress=0
	echo -ne "\r"\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ "\c"
	echo -ne "\r"$2 ["\c"
	for i in $(seq $1); do
		echo -n " "
	done
	echo -n "]"
	echo -ne "\r"$2 ["\c"
	while ps | grep $process > /dev/null && [ $progress -lt $1 ] ; do
		while [ $(cat apktool.log | wc -l) -gt $progress ]; do
			echo -n "#"
			let progress++
		done
	done
	wait
	echo -ne "\r"\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ "\r\c"
}


# Copie des fichiers modifie
# ---------------------------
 cp -r $Tools_Dir/add_fram/copie/* $Temp_Dir/framework-res > /dev/null 2>&1
 cp -r $Tools_Dir/add_fram/copie_$Size/* $Temp_Dir/framework-res > /dev/null 2>&1
 rm -f -r $Temp_Dir/framework-res/res/drawable-ldrtl-xhdpi > /dev/null 2>&1
 rm -f -r $Temp_Dir/framework-res/res/drawable-ldrtl-hdpi > /dev/null 2>&1

 
# Modification des fichiers
# --------------------------

# -res/values/dimens.xml
sed -e s/navigation_bar_height\"\>48/navigation_bar_height\"\>32/g \
    -e s/navigation_bar_height_landscape\"\>48/navigation_bar_height_landscape\"\>32/g \
    -e s/navigation_bar_width\"\>42/navigation_bar_width\"\>28/g \
    $Temp_Dir/framework-res/res/values/dimens.xml > $Temp_Dir/framework-res/res/values/dimens_temp.xml
mv -f $Temp_Dir/framework-res/res/values/dimens_temp.xml $Temp_Dir/framework-res/res/values/dimens.xml

# -res/values/styles.xml
head -n -1 $Temp_Dir/framework-res/res/values/styles.xml >  $Temp_Dir/framework-res/res/values/temp1.xml
sed -e /TextAppearance.StatusBar.EventContent.Title/,/style/d \
    $Temp_Dir/framework-res/res/values/temp1.xml > $Temp_Dir/framework-res/res/values/temp2.xml
cat $Temp_Dir/framework-res/res/values/temp2.xml $Tools_Dir/add_fram/modif/modif_styles.xml > $Temp_Dir/framework-res/res/values/styles.xml
rm -f $Temp_Dir/framework-res/res/values/temp*.xml

# -res/values/string.xml
head -n -1 $Temp_Dir/framework-res/res/values-fr/strings.xml >  $Temp_Dir/framework-res/res/values-fr/temp1.xml
sed -e /abbrev_wday_month_day_no_year/,/string/d \
    $Temp_Dir/framework-res/res/values-fr/temp1.xml > $Temp_Dir/framework-res/res/values-fr/temp2.xml
cat $Temp_Dir/framework-res/res/values-fr/temp2.xml $Tools_Dir/add_fram/modif/modif_strings.xml > $Temp_Dir/framework-res/res/values-fr/strings.xml
rm -f $Temp_Dir/framework-res/res/values-fr/temp*.xml

# Compile et Signe
# ----------------
apktool b -f $Temp_Dir/framework-res $Temp_Dir/MODframework-res_G.apk > apktool.log 2>&1 & show_apktool_progress 3 'Compilation'
	if grep "error" apktool.log  ; then  echo "$(date +\[%T\]) Erreur de decompilation de framework-res.apk" | tee -a $logfile ; cat apktool.log | tee -a $logfile; return 1; else rm -f apktool.log; fi #quitte en cas d'erreur

  
java -Xmx512m -jar $Tools_Dir/signapk.jar -w $Tools_Dir/testkey.x509.pem $Tools_Dir/testkey.pk8 $Temp_Dir/MODframework-res_G.apk $Temp_Dir/framework-res_G.apk > apktool.log 2>&1
	if grep "error" apktool.log  ; then  echo "$(date +\[%T\]) Erreur de signature de framework-res.apk" | tee -a $logfile ; cat apktool.log | tee -a $logfile; return 1; else rm -f apktool.log; fi #quitte en cas d'erreur


# Envoi de l'apk compile/signe dans la base
#------------------------------------------
cp -f $Temp_Dir/framework-res_G.apk $Work_Dir/Base_temp/gsteam/system/framework/framework-res.apk
