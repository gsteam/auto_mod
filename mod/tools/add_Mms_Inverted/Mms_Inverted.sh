#!/bin/sh

show_apktool_progress() {
	#Affiche la progression.
	#Parametres: i 'texte' ; i est un entier representant le nombre de lignes affichees par apktool
	process=$!
	progress=0
	echo -ne "\r"\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ "\c"
	echo -ne "\r"$2 ["\c"
	for i in $(seq $1); do
		echo -n " "
	done
	echo -n "]"
	echo -ne "\r"$2 ["\c"
	while ps | grep $process > /dev/null && [ $progress -lt $1 ] ; do
		while [ $(cat apktool.log | wc -l) -gt $progress ]; do
			echo -n "#"
			let progress++
		done
	done
	wait
	echo -ne "\r"\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ "\r\c"
}


# Cr�ation du dossier temp
# -------------------------
mkdir $Temp_Dir/Mms_Inverted

# Copie de l'apk neuf decompile
# ------------------------------
cp -r $Temp_Dir/Mms/* $Temp_Dir/Mms_Inverted

# Copie des fichiers modifie
# ---------------------------
cp -r $Tools_Dir/add_Mms_Inverted/copie/* $Temp_Dir/Mms_Inverted > /dev/null 2>&1
cp -r $Tools_Dir/add_Mms_Inverted/copie_$Size/* $Temp_Dir/Mms_Inverted > /dev/null 2>&1

# Modification des fichiers
# --------------------------

# -res/values/styles.xml
head -n -1 $Temp_Dir/Mms_Inverted/res/values/styles.xml > $Temp_Dir/Mms_Inverted/res/values/temp1.xml
sed -e /MmsHoloTheme/d \
    $Temp_Dir/Mms_Inverted/res/values/temp1.xml > $Temp_Dir/Mms_Inverted/res/values/temp2.xml
cat $Temp_Dir/Mms_Inverted/res/values/temp2.xml $Tools_Dir/add_Mms_Inverted/modifs/modif_styles.xml > $Temp_Dir/Mms_Inverted/res/values/styles.xml
rm -f $Temp_Dir/Mms_Inverted/res/values/temp*.xml

sed -e s/android:textColor\"\>#ff333333/android:textColor\"\>#ffffffff/g \
    -e s/android:textColor\"\>#ffcccccc/android:textColor\"\>#ffffffff/g \
    -e s/android:textColor\"\>#ff999999/android:textColor\"\>#ffffffff/g \
    -i $Temp_Dir/Mms_Inverted/res/values/styles.xml

# Compile et Signe
# ----------------
apktool b -f $Temp_Dir/Mms_Inverted $Temp_Dir/MODMms_Inverted.apk > apktool.log 2>&1 & show_apktool_progress 3 'Compilation'
	if grep "error" apktool.log  ; then  echo "$(date +\[%T\]) Erreur de decompilation de Mms_Inverted.apk" | tee -a $logfile ; cat apktool.log | tee -a $logfile; return 1; else rm -f apktool.log; fi #quitte en cas d'erreur


java -Xmx512m -jar $Tools_Dir/signapk.jar -w $Tools_Dir/testkey.x509.pem $Tools_Dir/testkey.pk8 $Temp_Dir/MODMms_Inverted.apk $Temp_Dir/Mms_Inverted.apk > apktool.log 2>&1
	if grep "error" apktool.log  ; then  echo "$(date +\[%T\]) Erreur de signature de Mms_Inverted.apk" | tee -a $logfile ; cat apktool.log | tee -a $logfile; return 1; else rm -f apktool.log; fi #quitte en cas d'erreur


# Envoi de l'apk compile/signe dans la base
#------------------------------------------
cp -f $Temp_Dir/Mms_Inverted.apk $Work_Dir/Base_temp/gsteam/Mms/Noir/app/Mms.apk
