#!/bin/sh

show_apktool_progress() {
	#Affiche la progression.
	#Parametres: i 'texte' ; i est un entier representant le nombre de lignes affichees par apktool
	process=$!
	progress=0
	echo -ne "\r"\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ "\c"
	echo -ne "\r"$2 ["\c"
	for i in $(seq $1); do
		echo -n " "
	done
	echo -n "]"
	echo -ne "\r"$2 ["\c"
	while ps | grep $process > /dev/null && [ $progress -lt $1 ] ; do
		while [ $(cat apktool.log | wc -l) -gt $progress ]; do
			echo -n "#"
			let progress++
		done
	done
	wait
	echo -ne "\r"\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ "\r\c"
}


# Extraction, transfert, decompile apk
# ------------------------------------
unzip -qq -j $Rom_Zip \
  system/app/ContactsProvider.apk \
  -d $Temp_Dir
apktool d -f $Temp_Dir/ContactsProvider.apk $Temp_Dir/ContactsProvider > apktool.log 2>&1 & show_apktool_progress 11 'Decompilation'
	if grep "error" apktool.log  ; then  echo "$(date +\[%T\]) Erreur de decompilation de ContactsProvider.apk" | tee -a $logfile ; cat apktool.log | tee -a $logfile; return 1; else rm -f apktool.log; fi #quitte en cas d'erreur

cd $Work_Dir/tools

# Copie des fichiers modifies
# ---------------------------
cp -r $Tools_Dir/add_ContactsProvider/copie/* $Temp_Dir/ContactsProvider > /dev/null 2>&1
cp -r $Tools_Dir/add_ContactsProvider/copie_$Size/* $Temp_Dir/ContactsProvider > /dev/null 2>&1

# Compile et Signe
# ---------------
apktool b -f $Temp_Dir/ContactsProvider $Temp_Dir/MODContactsProvider.apk > apktool.log 2>&1 & show_apktool_progress 3 'Compilation'
	if grep "error" apktool.log  ; then  echo "$(date +\[%T\]) Erreur de decompilation de ContactsProvider.apk" | tee -a $logfile ; cat apktool.log | tee -a $logfile; return 1; else rm -f apktool.log; fi #quitte en cas d'erreur


java -Xmx512m -jar $Tools_Dir/signapk.jar -w $Tools_Dir/testkey.x509.pem $Tools_Dir/testkey.pk8 $Temp_Dir/MODContactsProvider.apk $Temp_Dir/ContactsProvider.apk > apktool.log 2>&1
	if grep "error" apktool.log  ; then  echo "$(date +\[%T\]) Erreur de signature de ContactsProvider.apk" | tee -a $logfile ; cat apktool.log | tee -a $logfile; return 1; else rm -f apktool.log; fi #quitte en cas d'erreur


# Envoi de l'apk compile/signe dans la base
#------------------------------------------
cp -f $Temp_Dir/ContactsProvider.apk $Work_Dir/Base_temp/system/app/ContactsProvider.apk
