#!/bin/sh

show_apktool_progress() {
	#Affiche la progression.
	#Parametres: i 'texte' ; i est un entier representant le nombre de lignes affichees par apktool
	process=$!
	progress=0
	echo -ne "\r"\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ "\c"
	echo -ne "\r"$2 ["\c"
	for i in $(seq $1); do
		echo -n " "
	done
	echo -n "]"
	echo -ne "\r"$2 ["\c"
	while ps | grep $process > /dev/null && [ $progress -lt $1 ] ; do
		while [ $(cat apktool.log | wc -l) -gt $progress ]; do
			echo -n "#"
			let progress++
		done
	done
	wait
	echo -ne "\r"\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ "\r\c"
}


#-------------------------------------------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------- NORMAL ------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------------------------------------------------------------------------

# Copie des fichiers modifie
# ---------------------------
 cp -r $Tools_Dir/add_SystemUI_F/copie/* $Temp_Dir/SystemUI > /dev/null 2>&1
 cp -r $Tools_Dir/add_SystemUI_F/copie_$Size/* $Temp_Dir/SystemUI > /dev/null 2>&1

 
# Modification des fichiers
# --------------------------


# Compile et Signe
# ----------------
apktool b -f $Temp_Dir/SystemUI $Temp_Dir/MODSystemUI_F.apk > apktool.log 2>&1 & show_apktool_progress 3 'Compilation'
	if grep "error" apktool.log  ; then  echo "$(date +\[%T\]) Erreur de decompilation de SystemUI_F.apk" | tee -a $logfile ; cat apktool.log | tee -a $logfile; return 1; else rm -f apktool.log; fi #quitte en cas d'erreur


  
java -Xmx512m -jar $Tools_Dir/signapk.jar -w $Tools_Dir/testkey.x509.pem $Tools_Dir/testkey.pk8 $Temp_Dir/MODSystemUI_F.apk $Temp_Dir/SystemUI_F.apk > apktool.log 2>&1
	if grep "error" apktool.log  ; then  echo "$(date +\[%T\]) Erreur de signature de SystemUI_F.apk" | tee -a $logfile ; cat apktool.log | tee -a $logfile; return 1; else rm -f apktool.log; fi #quitte en cas d'erreur



# Envoi de l'apk compile/signe dans la base
#------------------------------------------
cp -f $Temp_Dir/SystemUI_F.apk $Work_Dir/Base_temp/gsteam/RomuMod/system/app/SystemUI.apk

#-------------------------------------------------------------------------------------------------------------------------------------------------------------
#-------------------------------------------------------------------- CENTRAGE DE L'HEURE --------------------------------------------------------------------
#-------------------------------------------------------------------------------------------------------------------------------------------------------------

# Copie des fichiers modifie Heure centr�e
# ---------------------------
 cp -r $Tools_Dir/add_SystemUI_F/copie_heure/* $Temp_Dir/SystemUI > /dev/null 2>&1
 
 # Compile et Signe
# ----------------
apktool b -f $Temp_Dir/SystemUI $Temp_Dir/MODSystemUI.apk > apktool.log 2>&1 & show_apktool_progress 3 'Compilation'
	if grep "error" apktool.log  ; then  echo "$(date +\[%T\]) Erreur de decompilation de SystemUI_F-centre.apk" | tee -a $logfile ; cat apktool.log | tee -a $logfile; return 1; else rm -f apktool.log; fi #quitte en cas d'erreur

  
java -Xmx512m -jar $Tools_Dir/signapk.jar -w $Tools_Dir/testkey.x509.pem $Tools_Dir/testkey.pk8 $Temp_Dir/MODSystemUI.apk $Temp_Dir/SystemUI.apk > apktool.log 2>&1
	if grep "error" apktool.log  ; then  echo "$(date +\[%T\]) Erreur de signature de SystemUI_F-centre.apk" | tee -a $logfile ; cat apktool.log | tee -a $logfile; return 1; else rm -f apktool.log; fi #quitte en cas d'erreur


# Envoi de l'apk compile/signe dans la base
#------------------------------------------
cp -f $Temp_Dir/SystemUI.apk $Work_Dir/Base_temp/gsteam/Heure/Romu/SystemUI.apk
