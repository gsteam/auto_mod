#!/bin/sh

show_apktool_progress() {
	#Affiche la progression.
	#Parametres: i 'texte' ; i est un entier representant le nombre de lignes affichees par apktool
	process=$!
	progress=0
	echo -ne "\r"\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ "\c"
	echo -ne "\r"$2 ["\c"
	for i in $(seq $1); do
		echo -n " "
	done
	echo -n "]"
	echo -ne "\r"$2 ["\c"
	while ps | grep $process > /dev/null && [ $progress -lt $1 ] ; do
		while [ $(cat apktool.log | wc -l) -gt $progress ]; do
			echo -n "#"
			let progress++
		done
	done
	wait
	echo -ne "\r"\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ "\r\c"
}


# Extraction, transfert, decompile apk
# ------------------------------------
unzip -qq -j $Rom_Zip \
  system/app/Contacts.apk \
  -d $Temp_Dir
apktool d -f $Temp_Dir/Contacts.apk $Temp_Dir/Contacts > apktool.log 2>&1 & show_apktool_progress 8 'Decompilation'
	if grep "error" apktool.log  ; then  echo "$(date +\[%T\]) Erreur de decompilation de Contacts.apk" | tee -a $logfile ; cat apktool.log | tee -a $logfile; return 1; else rm -f apktool.log; fi #quitte en cas d'erreur
cd $Work_Dir/tools

# Copie des fichiers modifies
# ---------------------------
cp -r $Tools_Dir/add_Contacts/copie/* $Temp_Dir/Contacts > /dev/null 2>&1
cp -r $Tools_Dir/add_Contacts/copie_$Size/* $Temp_Dir/Contacts > /dev/null 2>&1

# Modification des fichiers
# --------------------------

# -res/values/colors.xml
sed -e s/quickcontact_list_background\"\>#ffe2e2e2/quickcontact_list_background\"\>#ff000000/g \
    -e s/background_primary\"\>#ffffffff/background_primary\"\>#ff000000/g \
    -e s/background_social_updates\"\>#ffeeeeee/background_social_updates\"\>#ff000000/g \
    -e s/section_header_text_color\"\>#ff999999/section_header_text_color\"\>#ffbfbfbf/g \
    -e s/detail_update_tab_text_color\"\>#ff777777/detail_update_tab_text_color\"\>#ffffffff/g \
    -e s/dialtacts_secondary_text_color\"\>#ff888888/dialtacts_secondary_text_color\"\>#ffcfcfcf/g \
    -e s/contact_count_text_color\"\>#ffaaaaaa/contact_count_text_color\"\>#ffbfbfbf/g \
    -e s/stream_item_stripe_color\"\>#ffcccccc/stream_item_stripe_color\"\>#ff555555/g \
    -i $Temp_Dir/Contacts/res/values/colors.xml | tee -a $logfile

# -res/values/styles.xml
sed -e s/'android:textColor">\@\*android:color\/black'/'android:textColor">\@\*android:color\/white'/g \
    -e s/android:textColor\"\>#ff333333/android:textColor\"\>#ffcfcfcf/g \
    -e s/android:textColor\"\>#cdffffff/android:textColor\"\>#ffdfdfdf/g \
    -e s/\Widget.Holo.Light.ActionBar.Solid.Inverse/Widget.Holo.ActionBar.Solid/g \
    -e s/Theme.Holo.Light.DarkActionBar/Theme.Holo/g \
    -e s/Holo.Light/Holo/g \
    -i $Temp_Dir/Contacts/res/values/styles.xml  | tee -a $logfile

# Compile et Signe
# ---------------
apktool b -f $Temp_Dir/Contacts $Temp_Dir/MODContacts.apk > apktool.log 2>&1 & show_apktool_progress 3 'Compilation'
	if grep "error" apktool.log  ; then  echo "$(date +\[%T\]) Erreur de compilation de Contacts.apk" | tee -a $logfile ; cat apktool.log | tee -a $logfile; return 1; else rm -f apktool.log; fi #quitte en cas d'erreur

java -Xmx512m -jar $Tools_Dir/signapk.jar -w $Tools_Dir/testkey.x509.pem $Tools_Dir/testkey.pk8 $Temp_Dir/MODContacts.apk $Temp_Dir/Contacts.apk> apktool.log 2>&1
	if grep "error" apktool.log  ; then  echo "$(date +\[%T\]) Erreur de Signature de Contacts.apk" | tee -a $logfile ; cat apktool.log | tee -a $logfile; return 1; else rm -f apktool.log; fi #quitte en cas d'erreur

# Envoi de l'apk compile/signe dans la base
#------------------------------------------
cp -f $Temp_Dir/Contacts.apk $Work_Dir/Base_temp/system/app/Contacts.apk
