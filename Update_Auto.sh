 #!/bin/sh
unset DISPLAY
# ================================================================================================================
# |||||||||||||||||||||||||||||||||             Parametres du Prog            ||||||||||||||||||||||||||||||||||||
# ================================================================================================================

# Variables internes
#WebPath1=/home/gsteam/public_html/beta/Beta
WebPath2=/home/gsteam/public_html/download/Releases
waitTime=60 #Intervalle de scrutation serveur Cyano en secondes
archiveTime=5 #Temps de conservation des Archives sur le mirroir (en jours)

# ================================================================================================================
# |||||||||||||||||||||||||     Declaration des variables avant Prog principal    ||||||||||||||||||||||||||||||||
# ================================================================================================================

# Variables globales: Dossiers base, tools et temp
export Destination_Dir="$(pwd)"
export Work_Dir="$Destination_Dir/mod"
export Temp_Dir="$Work_Dir/tools/temp"
export Tools_Dir="$Work_Dir/tools"


# ================================================================================================================
# ||||||||||||||||||||||||||||||||||||||||||||||      Outils      ||||||||||||||||||||||||||||||||||||||||||||||||
# ================================================================================================================

check_JDK(){
	# usrc=9
	# heapy=64

	echo "$(date +\[%T\]) Verification du Java Development Kit" | tee -a $logfile
	java -version > /dev/null
	if [ $? != 0 ]; then 
	  echo "$(date +\[%T\]) Le JDK (Java) n'est pas installe. Telechargez le JDK Java SE 7u1"  | tee -a $logfile
	  exit
	fi
}
check_Xfvb() {
	echo "$(date +\[%T\]) Verification du serveur X virtuel" | tee -a $logfile
	#Verifie serveur X virtuel et lance si besoin
	if ! ps -ef|grep Xvfb | grep -v "grep" > /dev/null; then
			/usr/bin/Xvfb :1 -screen 0 1024x768x16 > /dev/null 2>&1 &
	fi
	#Recupere le numero de DISPLAY du serveur X virtuel
	export DISPLAY="localhost$( ps -ewwo args | grep Xvfb | grep -v grep | sed 's| |%|g' | cut -d'%' -f2).$( ps -ewwo args | grep Xvfb | grep -v grep | sed 's| |%|g' | cut -d'%' -f4)"
}
get_Rom_Properties() {
	filename_parsed=$(echo $Rom_Zip | sed 's|cm|%cm|' | sed 's|$device||' | sed 's|$type||' | sed 's|\-|%|g' | sed 's|\.zip|%|g' )
	romname=$(echo $filename_parsed | cut -d '%' -f2)
	romversion=$(echo $filename_parsed | cut -d '%' -f3)
	romdate=$(echo $filename_parsed | cut -d '%' -f4)
	export build=$(echo "$romname-$romversion-$romdate")
	export modversion=$(echo $romversion-$romdate-$type-$device)
	
	case $device in
		"galaxysmtd" | "i9100" 	) export Size="hdpi" ;;
		"i9300" | "i9305" 		) export Size="xhdpi" ;;
		* 						) export Size=""; echo "$(date +\[%T\]) Erreur: device non supporté" | tee -a $logfile ; cat apktool.log | tee -a $logfile; return 1;; #quitte en cas d'erreur 
	esac
}
log_append_auto_2_main(){
	cat $auto_logfile >> $main_logfile
}
log_start_main(){
	clear
	export main_logfile="$Destination_Dir/main.log"
	if [ -f $main_logfile ] ; then rm $main_logfile ; fi
	touch $main_logfile
	export logfile=$main_logfile
	echo "" | tee -a $logfile
	echo "  $(date)" | tee -a $logfile
	echo " ------------------------------------------" | tee -a $logfile
	echo "|                Démarrage                 |" | tee -a $logfile
	echo " ------------------------------------------" | tee -a $logfile
	echo "" | tee -a $logfile
}
log_start_manu(){
	clear
	# Log
	export manu_logfile="$Destination_Dir/manu.log"
	if [ -f $manu_logfile ] ; then rm $manu_logfile ; fi
	touch $manu_logfile
	export logfile=$manu_logfile
	echo "  $(date)" | tee -a $logfile
	echo " ------------------------------------------" | tee -a $logfile
	echo "|          Lancement de ManuMod            |" | tee -a $logfile
	echo " ------------------------------------------" | tee -a $logfile
	echo "" | tee -a $logfile
}
log_start_auto(){
	export auto_logfile="$Destination_Dir/auto.log"
	if [ -f $auto_logfile ] ; then
		if $append_auto_2_main ; then log_append_auto_2_main; fi
		append_auto_2_main=false
		rm $auto_logfile > /dev/null
	fi
	touch $auto_logfile
	export logfile=$auto_logfile
}
log_restart_update(){
	# Log
	export update_logfile="$Destination_Dir/GSTeam-Moder-$build-$device.log"
	cp -f $logfile $update_logfile
	export logfile=$update_logfile
	echo "" | tee -a $logfile
	echo " ------------------------------------------" | tee -a $logfile
	echo "|             Creation du mod              |" | tee -a $logfile
	echo " ------------------------------------------" | tee -a $logfile
	echo "" | tee -a $logfile
}
log_switchto_auto(){
	export logfile=$auto_logfile
}
log_switchto_main(){
	export logfile=$main_logfile
}
mrpropre() { 
	echo "$(date +\[%T\]) Nettoyage des fichiers..." | tee -a $logfile
	rm -f -r $Work_Dir/Base_temp
	rm -f -r $Temp_Dir
}
show_countdown() {
    i=$1
    while [[ $i -ge 0 ]]
      do
        echo -e "\r "Attente nouvelle version, synchronisation dans $i secondes...\ \ \ \ \ \ \ \ \ \ \ " \c"
        sleep 1
        i=$(expr $i - 1)
    done
}
show_apktool_progress() {
	#Affiche la progression.
	#Parametres: i 'texte' ; i est un entier representant le nombre de lignes affichees par apktool
	process=$!
	progress=0
	echo -ne "\r"\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ "\c"
	echo -ne "\r"$2 ["\c"
	for i in $(seq $1); do
		echo -n " "
	done
	echo -n "]"
	echo -ne "\r"$2 ["\c"
	while ps | grep $process > /dev/null && [ $progress -lt $1 ] ; do
		while [ $(cat apktool.log | wc -l) -gt $progress ]; do
			echo -n "#"
			let progress++
		done
	done
	wait
	echo -ne "\r"\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ "\r\c"
}
# ================================================================================================================
# |||||||||||||||||||||||||||||||||               Update_Auto                 ||||||||||||||||||||||||||||||||||||
# ================================================================================================================

Update_Auto(){
	# ================================================================================================================
	# |||||||||||||||||||||||||||||||||             Parametres du Prog            ||||||||||||||||||||||||||||||||||||
	# ================================================================================================================
	
	# Emplacement et nom du zip de la Rom
	#Fait dans get_Rom_Properties
	
	# Device concerné
	#Fait dans get_Rom_Properties
	

	# Définition d'affichage, (size=hdpi if galaxysmtd/i9100, size=xhdpi if i9300)
	#Fait dans get_Rom_Properties
	
	# Nom du kernel à ajouter au Mod
	kernel=$(ls $Work_Dir/Base_$device/gsteam/Kernels/)
	if [ $(echo $kernel | wc -w) != 1 ]; then echo "$(date +\[%T\]) Erreur: Impossible de choisir un kernel dans Base_$device/gsteam/Kernel/" | tee -a $logfile ; cat apktool.log | tee -a $logfile; return 1;fi #quitte en cas d'erreur

	# Version kernel
	versionkernel=$(ls $Work_Dir/Base_$device/gsteam/Kernels/$kernel)
	if [ $(echo $versionkernel | wc -w) != 1 ]; then echo "$(date +\[%T\]) Erreur: Impossible de choisir un kernel dans Base_$device/gsteam/Kernel/$kernel" | tee -a $logfile ; cat apktool.log | tee -a $logfile; return 1;fi #quitte en cas d'erreur 

	#fichier pour md5
	md5file="$Destination_Dir/GSTeam-Moder-$build-$device.md5"

	# ================================================================================================================
	# ||||||||||||||||||||||||||||||||||||||    Nettoyage des fichiers     |||||||||||||||||||||||||||||||||||||||||||
	# ================================================================================================================

	mrpropre 


	# ================================================================================================================
	# ||||||||||||||||||||||||||||||||||||||    Préparation de la base      ||||||||||||||||||||||||||||||||||||||||||
	# ================================================================================================================

	# Mise a jour des scripts via git
	#--------------------------------
	git stash
	git pull origin
	# Attention Update_Auto.sh doit être relancé pour prendre en compte les modifications de celui-ci. 
	# C'est automatique pour les scripts apk

	# Création dossier temp de travail
	#---------------------------------
	mkdir -p $Work_Dir/tools/temp
	mkdir -p $Work_Dir/Base_temp
	cp -r $Work_Dir/Base/* $Work_Dir/Base_temp
	cp -r $Work_Dir/Base_$device/* $Work_Dir/Base_temp
	find  $Work_Dir/Base_temp -type f -name ".placeholder"  -exec rm {} \;

	# Extraction des fichiers du ZIP
	#-------------------------------

	echo "$(date +\[%T\]) Extraction des fichiers" | tee -a $logfile

	unzip -qq -j $Rom_Zip \
		system/build.prop \
		system/framework/framework-res.apk \
		-d $Temp_Dir
	if [ $? != 0 ] ; then echo "$(date +\[%T\]) Erreur: Erreur d'extraction de $RomZip" | tee -a $logfile ; cat apktool.log | tee -a $logfile; return 1;fi #quitte en cas d'erreur
	echo "$(date +\[%T\]) Creation du patch..." | tee -a $logfile

	# Telechargement xtras
	#---------------------
	echo "$(date +\[%T\]) Telechargement xtras et hosts" | tee -a $logfile
	wget -q -P $Temp_Dir http://xtra2.gpsonextra.net/xtra.bin
	if [ $? != 0 ]  ; then echo "$(date +\[%T\]) Erreur: Erreur de telechargement xtras et hosts" | tee -a $logfile ; cat apktool.log | tee -a $logfile; return 1;fi #quitte en cas d'erreur

	# Install Framework pour ApkTool
	#-------------------------------
	echo "$(date +\[%T\]) Installation du Framework " | tee -a $logfile
	apktool if $Temp_Dir/framework-res.apk > apktool.log 2>&1 & show_apktool_progress 1 'Installation du framework'
	if [ $? != 0 ]  ; then  echo "$(date +\[%T\]) Erreur d'installation du Framework" | tee -a $logfile ; cat apktool.log | tee -a $logfile; return 1; else rm -f apktool.log; fi #quitte en cas d'erreur
	apktool d -f $Temp_Dir/framework-res.apk $Temp_Dir/framework-res  > apktool.log 2>&1 & show_apktool_progress 8 'Decompilation du framework'
	if [ $? != 0 ]  ; then  echo "$(date +\[%T\]) Erreur d'installation du Framework" | tee -a $logfile ; cat apktool.log | tee -a $logfile; return 1; else rm -f apktool.log; fi #quitte en cas d'erreur

	# ================================================================================================================
	# |||||||||||||||||||||||||||      Modification et compilation des fichiers      |||||||||||||||||||||||||||||||||
	# ================================================================================================================

	# Ajout de la barre GN
	#---------------------
	echo "$(date +\[%T\]) Ajout de la barre GN" | tee -a $logfile
	cat $Temp_Dir/build.prop $Tools_Dir/nav_on.prop > $Temp_Dir/build_nav_on.prop
	cat $Temp_Dir/build.prop $Tools_Dir/nav_off.prop > $Temp_Dir/build_nav_off.prop

	# Lancement des scripts apk
	#--------------------------
	find $Tools_Dir/add_* -name "*.sh" | while read shfile; do 
		echo "$(date +\[%T\]) Application du script $(basename $shfile)"| tee -a $logfile
		rm -f /tmp/APKTOOL*
		chmod +x $shfile
		source $shfile
		if [ $? != 0 ]  ; then
			echo "$(date +\[%T\]) Erreur d'execution du script $(basename $shfile)" | tee -a $logfile
			#return 1; ne quitte plus en cas d'erreur
		fi
	done
	if [ $? != 0 ]  ; then  return 1; fi

	# ================================================================================================================
	# |||||||||||||||||||||||||||||||||||||||||       Creation du zip        ||||||||||||||||||||||||||||||||||||||||
	# ================================================================================================================

	# Copie fichiers
	#-----------------
	cp -f $Temp_Dir/build_nav_off.prop $Work_Dir/Base_temp/system/build.prop
	cp -f $Temp_Dir/build_nav_on.prop $Work_Dir/Base_temp/gsteam/Barre/build.prop
	cp -f $Temp_Dir/xtra.bin $Work_Dir/Base_temp/data/xtra.bin

	# Preparation Aroma
	#------------------

	cp $Tools_Dir/aroma-config-orig $Work_Dir/Base_temp/META-INF/com/google/android/aroma-config  
	sed -e "s|\(rom_date[^a-z]*\)\(xxxx\)|\1$build|g" \
	  -e "s|\(rom_device[^a-z]*\)\(xxxx\)|\1$device|g" \
	  -e "s|\(ajoutkernel\)|$kernel|g" \
	  -e "s|\(versionkernel\)|$versionkernel|g" \
	  -i $Work_Dir/Base_temp/META-INF/com/google/android/aroma-config  

	# Preparation Updater
	#--------------------
	cp $Tools_Dir/updater-script-orig $Work_Dir/Base_temp/META-INF/com/google/android/updater-script 
	sed -e "s|\(montage\)|$(cat $Tools_Dir/mount$device)|g" \
	  -e "s|\(flashkernel\)|$(cat $Tools_Dir/flashkernel$device)|g" \
	  -i $Work_Dir/Base_temp/META-INF/com/google/android/updater-script 

	# Creation du ZIP
	#------------------
	echo "$(date +\[%T\]) Creation du ZIP" | tee -a $logfile

	cp -r $Work_Dir/Base_temp/gsteam/Kernels/$kernel/$versionkernel/* $Work_Dir/Base_temp/gsteam/Kernels
	rm -f -r $Work_Dir/Base_temp/gsteam/Kernels/$kernel
	cd $Work_Dir/Base_temp
	zip -qq -r $Destination_Dir/GSTeam-Moder-$build-$device.zip * -x .placeholder
	cd $Destination_Dir

	# ================================================================================================================
	# ||||||||||||||||||||||||||||||||||      Nettoyage des fichiers      ||||||||||||||||||||||||||||||||||||||||||||
	# ================================================================================================================

	mrpropre 

	# ================================================================================================================
	# ||||||||||||||||||||||||||||||||||               Fin                ||||||||||||||||||||||||||||||||||||||||||||
	# ================================================================================================================
	echo "$(date +\[%T\]) Creation du mod terminee" | tee -a $logfile
	cd $Destination_Dir
	md5sum GSTeam-Moder-$build-$device.zip > $md5file
	echo "$(date +\[%T\]) MD5: $(cat $md5file)" | tee -a $logfile
}

# ================================================================================================================
# ||||||||||||||||||||||||||||||||||||||||||       auto_mod        |||||||||||||||||||||||||||||||||||||||||||||||
# ================================================================================================================
Auto_Mod(){
	echo "" | tee -a $logfile
	echo "  $(date)" | tee -a $logfile
	echo " ------------------------------------------" | tee -a $logfile
	echo "|    Synchronisation avec serveur Cyano    |" | tee -a $logfile
	echo " ------------------------------------------" | tee -a $logfile
	echo "" | tee -a $logfile
	while :
	do
		#scrutation beta et release
		for WebPath in $WebPath1 $WebPath2; do
			#log temporaire (initialisation et mirroir)
			log_start_auto
			#Recherche rom cyano, device et type dans WebPath
			find $WebPath -mindepth 3 -maxdepth 3 -type d | grep "CyanogenMod/"  | while read dossier; do 
					#Extrait les propriétés de la rom d'après le chemin
					dossier=$(echo ${dossier:${#WebPath}+1} | sed 's|\/|%|g')
					dateVerif=$(date +\[%T\])
					export device=$(echo $dossier | cut -d'%' -f1)
					#export BaseRom=$(echo $dossier | cut -d'%' -f2)
					export type=$(echo $dossier | cut -d'%' -f3)
					if [ -z $device ] || [ -z $type ] ; then
						echo "$dateVerif Entre dans $WebPath" | tee -a $logfile
						echo "$(date +\[%T\]) Erreur: device=$device ou type=$type incorrect" | tee -a $logfile
						append_auto_2_main=true && continue
					fi # dossier suivant si pas de device ou de type
					echo -e "\r "Verification du serveur cyano: Rom $type pour $device...\ \ \ \ \ \ \ \ \ \ \ "  \c"
					NewROMUrl=$(curl -s "http://download.cyanogenmod.com/?type=$type&device=$device" \
						| grep 'cm-10.1' | grep '.zip' | head -n1 | sed 's|data.url=|%|' | sed 's|\&data.name|%|' | cut -d'%' -f3)
					if [ -z $NewROMUrl ] ; then
						# echo "$(date +\[%T\]) Erreur: Pas de $type trouvee pour $device" | tee -a $logfile
						append_auto_2_main=true && continue
					fi # dossier suivant si pas de cm10.1 trouvée pour le device

					# Nom du zip de la derniere cyano
					NewROMFile=$(echo $NewROMUrl | sed 's|/|%|g' | cut -d'%' -f7)
					# Préparation des variables pour Update_Auto
					export Rom_Zip="$WebPath/$device/CyanogenMod/$type/$NewROMFile"
					get_Rom_Properties					 
					if [ -f $WebPath/$device/CyanogenMod/$type/$NewROMFile ] && [ -f $WebPath/$device/CyanogenMod/$type/GSTeam-Moder-$build-$device.zip ] ; then
						continue
					fi # dossier suivant si NewRomFile et mod déjà sur le site
					append_auto_2_main=true
					echo -e "\n"$(date +\[%T\]) Nouvelle Cyano $build-$type trouvee pour $device"\n" | tee -a $logfile
					rm -f $WebPath/$device/CyanogenMod/$type/GSTeam-Moder-$build-$device.zip > /dev/null
					rm -f $WebPath/$device/CyanogenMod/$type/$NewROMFile  > /dev/null
			
					#Téléchargement et lancement d'Update_Auto si téléchargement OK
					echo "$(date +\[%T\]) Telechargement de $NewROMUrl" | tee -a $logfile
					if ! wget -P $WebPath/$device/CyanogenMod/$type -N --progress=bar:force "$NewROMUrl"  | tee -a $logfile || ! [ -f $WebPath/$device/CyanogenMod/$type/$NewROMFile ]; then
						append_auto_2_main=true
						echo "$(date +\[%T\]) Erreur de téléchargement de $NewROMUrl" | tee -a $logfile
						rm -f $WebPath/$device/CyanogenMod/$type/$NewROMFile
						continue
					fi # dossier suivant si échec telechargement
					
					#Archivage
					if ! [ -d $WebPath/$device/CyanogenMod/$type/Archives ]; then mkdir $WebPath/$device/CyanogenMod/$type/Archives; fi # créé Archives si besoin
					echo "$(date +\[%T\]) Archivage des dernieres rom et mod"  | tee -a $logfile
					find $WebPath/$device/CyanogenMod/$type/Archives -type f -mtime +$archiveTime -exec rm {} \; # Suppression des archives de plus de $archiveTime jours
					find $WebPath/$device/CyanogenMod/$type -maxdepth 1 -type f ! -name $NewROMFile -exec mv -f {} $WebPath/$device/CyanogenMod/$type/Archives/ \; #Archivage des fichiers courants
					
					# Lancement Update_Auto et si non interrompu alors mise en ligne des fichiers
					echo "$(date +\[%T\]) Creation du mod GSTeam-Moder-$build-$device.zip" | tee -a $logfile
					log_restart_update
					if Update_Auto; then 
						echo "$(date +\[%T\]) Mise en ligne" | tee -a $logfile
						log_switchto_auto
						echo "$(date +\[%T\]) Mise en ligne de GSTeam-Moder-$build-$device.zip" >> $logfile
						mv -f $Destination_Dir/GSTeam-Moder-$build-$device.* $WebPath/$device/CyanogenMod/$type/
					else
						echo "$(date +\[%T\]) Fatal: Erreur de creation du mod" | tee -a $logfile
						log_switchto_auto
						append_auto_2_main=true
						echo "$(date +\[%T\]) Fatal: Erreur de creation du mod GSTeam-Moder-$build-$device.zip" >> $logfile							
#						mv -f $logfile $WebPath/$device/CyanogenMod/$type/
					fi
					echo "" | tee -a $logfile
					echo "  $(date)" | tee -a $logfile
					echo " ------------------------------------------" | tee -a $logfile
					echo "|    Synchronisation avec serveur Cyano    |" | tee -a $logfile
					echo " ------------------------------------------" | tee -a $logfile
					echo "" | tee -a $logfile
			done # find dossier
		done # for WebPath
		show_countdown $waitTime
	done
}

# ================================================================================================================
# ||||||||||||||||||||||||||||||||||||||||||       manu_mod        |||||||||||||||||||||||||||||||||||||||||||||||
# ================================================================================================================
Manu_Mod(){
	log_start_manu
	# Préparation des variables pour Update_Auto
	export Rom_Zip=$1
	get_Rom_Properties
	
	# Lancement Update_Auto
	echo "$(date +\[%T\]) Creation du mod $build-$type pour $device" | tee -a $logfile

	log_restart_update
	if ! Update_Auto; then #Si erreur
		echo "$(date +\[%T\]) Fatal: Erreur de creation du mod" | tee -a $logfile
		log_switchto_main
		echo "$(date +\[%T\]) Fatal: Erreur de creation du mod" >> $logfile
		break							
	fi
	rm $manu_logfile
}

log_start_main
check_JDK
#check_Xfvb

# Si argument...
if [ $# != 0 ]; then
		#si mrpropre..
        if [ $1 == "mrpropre" ]; then
                mrpropre
        #..sinon Manu_Mod
        else
                echo "$(date +\[%T\]) Lancement de Manu_Mod" | tee -a $logfile
                if echo $1 | grep "/" ; then
                        Rom_Zip=$1
                else
                        Rom_Zip=$(pwd)/$1
                fi
                if ! [ -f $Rom_Zip ] ; then 
                        echo "$(date +\[%T\]) Erreur: Argument incorrect" | tee -a $logfile
                else
                        Manu_Mod $Rom_Zip
                fi
        fi
else #... sinon lance boucle auto
        echo "$(date +\[%T\]) Lancement d'Auto_Mod" | tee -a $logfile
        Auto_Mod  
fi
